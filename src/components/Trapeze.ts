import Shapes from "./abstract/shapes";

export default class Trapeze extends Shapes {
    a: number;
    b: number;
    c: number;
    d: number;
    constructor(a: number, b: number, c: number, d: number) {
        super();
        const p = (a + b + c + d) / 3;
        if (a + b !== c + d || d > a+b+c || c > a+b+d || b > a+c+d || a > b+c+d) {
            throw new Error("в такую фигуру нельзя вписать окружность ( a+ b = c+ d). Такой Трапеции не может существовать!");
        }
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    area() {
        /* формула = (a+b)/2 * √(c^2 - {((a-b)^2 + c^2 -d^2) / 2(a-b)}^2) */
        const differenceAB = this.a - this.b; 
        const powC = this.c**2;
        const rightSquareRoot = ((differenceAB**2 + powC - this.d**2) / (2 * differenceAB))**2; //4/-4 = 
        const squareRoot = Math.sqrt(powC - rightSquareRoot);

        return ((this.a + this.b) / 2) * squareRoot;
    }

    perimetr() {
        return this.a + this.b + this.c + this.d;
    }
}