import Shapes from "./abstract/shapes";

export default class Circle extends Shapes {
    radius: number
    constructor(radius: number) {
        super();
        this.radius = radius;
    }

    area() {
        return this.radius**2 * Math.PI;
    }

    perimetr() {
        return this.radius * Math.PI * 2
    }
}