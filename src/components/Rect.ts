import Shapes from "./abstract/shapes";

export default class Rect extends Shapes {
    width: number;
    height: number;
    constructor(width: number, height: number) {
        super();
        this.width = width;
        this.height = height;
    }

    area() {
        return this.width*this.height;
    }

    perimetr() {
        return 2 * (this.width + this.height);
    }
}