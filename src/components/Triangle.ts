import Shapes from "./abstract/shapes";

/*
    * полупериметр  p = (a+b+c)/2 
    * s =√(p(p-a)(p-b)(p-c))
*/

export default class Triangle extends Shapes {
    a: number;
    b: number;
    c: number;
    p: number;

    constructor(a: number, b: number, c: number) {
        super();
        if (c > a + b || b > a + c || a > b + c) {
            throw new Error("Одна сторона треугольника больше или равна суммы двух других сторон. Такой треугольник не может существовать!");
        }

        this.a = a;
        this.b = b;
        this.c = c;
    }

    area() {
        const p = (this.a + this.b + this.c) / 2
        return Math.sqrt(p * (p - this.a) * (p - this.b) * (p - this.c));
    }

    perimetr() {
        return this.a + this.b + this.c;
    }
}