import Shapes from "./abstract/shapes";

export default class Square extends Shapes  {
    width: number;
    constructor(width: number) {
        super();
        this.width = width;
    }

    area() {
        return this.width**2; // *Возведение в степень
    }

    perimetr() {
        return 4*this.width;
    }
}