import Shapes from "./shapes"

type jsonShapes = {
    [key: string] : number;
    area: number;
    perimetr: number;
}

export default  class PrintShapes {
  shape: Shapes
  constructor(shape) {
    this.shape = shape;
  }

  json(): jsonShapes {
    const area = this.shape.area();
    const perimetr = this.shape.perimetr();

    let obj = {
      area,
      perimetr
    };

    Object.keys(this.shape).forEach(key => {
      obj[key] = this.shape[key];
    });

    return obj
  }
}