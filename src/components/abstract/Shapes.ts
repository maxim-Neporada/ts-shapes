export default abstract class Shapes {
    abstract area(): number; 
    abstract perimetr(): number;
}