/* Главный файл по выводу расчетов площади и создание фигур */
import Square from "./components/square";
import Rect from "./components/Rect";
import Circle from "./components/Cirlcle";
import Triangle from "./components/Triangle";
import Trapeze from "./components/Trapeze";
import PrintShapes from "./components/abstract/PrintShapes";

const squareFirst = new Square(20);
const rectFirst = new Rect(20, 25);
const circle = new Circle(5);
const triangle = new Triangle(1, 3, 3);
const trapeze = new Trapeze(1, 3, 2, 2);

const printerSquare = new PrintShapes(squareFirst);
console.log("printerSquare", printerSquare.json());

const printerRectFirst = new PrintShapes(rectFirst);
console.log(" printerRectFirst", printerRectFirst.json());

const printerCircle = new PrintShapes(circle);
console.log(" printerCircle", printerCircle.json());

const printerTriangle = new PrintShapes(triangle);
console.log(" printerTriangle", printerTriangle.json());

const printerTrapeze = new PrintShapes(trapeze);
console.log("printerTrapeze", printerTrapeze.json());




